package lab01.example.model;

import sun.java2d.pipe.SpanShapeRenderer;

public abstract class SimpleBankAccountWithAtm extends SimpleBankAccount implements BankAccountWithAtm  {

    private AccountHolder holder;
    private double amount;

    public SimpleBankAccountWithAtm(final AccountHolder holder, final double amount){
        super(holder, amount);
    }

    public abstract int operationFee();

    @Override
    public void atmWithdraw(int usrID, double amount) {
        withdraw(usrID, amount + operationFee());
    }

}
