package lab01.example.model;

public interface BankAccountWithAtm {

    void atmWithdraw(int usrID, double amount);

}
