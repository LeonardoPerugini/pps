package lab01.tdd;

import lab01.tdd.strategies.SelectStrategy;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class CircularListImpl implements CircularList {

    final private List<Integer> list = new LinkedList<Integer>();
    private Integer current = null;

    @Override
    public void add(int element) {
        this.list.add(element);
        if(this.current==null)
            this.current = 0;
    }

    @Override
    public int size() {
        return this.list.size();
    }

    @Override
    public boolean isEmpty() {
        return this.list.isEmpty();
    }

    @Override
    public Optional<Integer> next() {
        Integer element;
        if(!isEmpty()) {
            if(this.current == (this.size() -1))
                this.current = 0;
            else
                this.current = this.current + 1;

            element = this.list.get(current);
            return Optional.of(element);
        }
        return Optional.empty();
    }

    @Override
    public Optional<Integer> previous() {
        if(!isEmpty()){
            if(this.current == 0) {
                this.current = this.list.size() - 1;
                return Optional.of(this.list.get(current));
            }
            this.current = this.current - 1;
            return Optional.of(this.list.get(current));
        }
        return Optional.empty();
    }

    @Override
    public void reset() {
        this.list.clear();
        this.current = null;
    }

    @Override
    public Optional<Integer> next(SelectStrategy strategy) {
        if (isEmpty())
            return Optional.empty();

        while (!strategy.apply(list.get(current))) {
            current = (current + 1) % size();
        }
        Optional<Integer> el = Optional.of(list.get(current));
        current = (current + 1) % size();
        return el;
    }

}
