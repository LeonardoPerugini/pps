package lab01.tdd.strategies;

public final class SelectStrategyFactoryImpl implements SelectStrategyFactory {

    @Override
    public SelectStrategy createEqualsStrategy(int element) {
        return i -> (i == element);
    }

    @Override
    public SelectStrategy createMultipleOfStrategy(int element) {
        return i -> (i%element==0);
    }

    @Override
    public SelectStrategy createEvenStrategy() { return i -> (i % 2 == 0); }
}
