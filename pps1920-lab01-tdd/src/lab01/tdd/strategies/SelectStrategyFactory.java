package lab01.tdd.strategies;

public interface SelectStrategyFactory {

    SelectStrategy createEqualsStrategy(final int element);
    SelectStrategy createMultipleOfStrategy(final int element);
    SelectStrategy createEvenStrategy();

}
