import lab01.tdd.CircularList;
import lab01.tdd.CircularListImpl;
import lab01.tdd.strategies.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The test suite for testing the CircularList implementation
 */
public class CircularListTest {

    private CircularList list;

    @BeforeEach
    void beforeEach(){
        list = new CircularListImpl();
    }

    @Test
    void testAdd(){
        list.add(1);
        assertEquals(list.size(), 1);
    }

    @Test
    void testSize(){
        fill(3);
        assertEquals(this.list.size(), 3);
    }

    @Test
    void testIsEmpty(){
        assertTrue(this.list.isEmpty());
    }

    @Test
    void testNext(){
        fill(1);
        assertTrue(this.list.next().isPresent());
        assertEquals(1, this.list.next().isPresent());
    }

    @Test
    void testNextExact(){
        fill(3);
        assertEquals(this.list.next().get(), 2);
    }

    @Test
    void testPrevious(){
        fill(3);

        assertTrue(this.list.previous().isPresent());
    }

    @Test
    void testPreviousExact(){
        fill(3);
        assertEquals(this.list.previous().get(), 3);
    }

    @Test
    void testReset(){
        fill(3);
        assertFalse(this.list.isEmpty());
        this.list.reset();
        assertTrue(this.list.isEmpty());
    }

    @Test
    void testEvenStrategy(){
        fill(6);
        /* even strategy */
        assertEquals(2, this.list.next(n -> n%2 == 0).get());
        assertEquals(4, this.list.next(n -> n%2 == 0).get());
        assertEquals(6, this.list.next(n -> n%2 == 0).get());
    }

    @Test
    void testEvenStrategyWithFactory(){
        fill(6);
        /* even strategy */
        assertEquals(2, this.list.next(new SelectStrategyFactoryImpl().createEvenStrategy()).get());
        assertEquals(4, this.list.next(new SelectStrategyFactoryImpl().createEvenStrategy()).get());
        assertEquals(6, this.list.next(new SelectStrategyFactoryImpl().createEvenStrategy()).get());
    }

    @Test
    void testMultipleOfStrategy(){
        fill(6);
        /* multiple strategy */
        final int element = 3;
        assertEquals(3, this.list.next(n -> n%element == 0).get());
        assertEquals(6, this.list.next(n -> n%element == 0).get());
        assertEquals(3, this.list.next(n -> n%element == 0).get());
    }

    @Test
    void testMultipleOfStrategyWithFactory(){
        fill(6);        /* multiple strategy */
        final int element = 3;
        assertEquals(3, this.list.next(new SelectStrategyFactoryImpl().createMultipleOfStrategy(element)).get());
        assertEquals(6, this.list.next(new SelectStrategyFactoryImpl().createMultipleOfStrategy(element)).get());
        assertEquals(3, this.list.next(new SelectStrategyFactoryImpl().createMultipleOfStrategy(element)).get());
    }

    @Test
    void testEqualsStrategy(){
        fill(6);        /* multiple strategy */
        final int element = 5;
        assertEquals(5, this.list.next(n -> n == element).get());
        assertEquals(5, this.list.next(n -> n == element).get());
    }

    @Test
    void testEqualsStrategyWithFactory(){
        fill(6);        /* multiple strategy */
        final int element = 5;
        assertEquals(5, this.list.next(new SelectStrategyFactoryImpl().createEqualsStrategy(element)).get());
        assertEquals(5, this.list.next(new SelectStrategyFactoryImpl().createEqualsStrategy(element)).get());
    }

    private void fill(int elements){
        for(int i = 1; i <= elements; i++){
            this.list.add(i);
        }
    }

}

/*
usando una classe per ogni strategia (ogni classe che implementa SelectStrategy e di conseguenza il metodo apply)

    @Test
    void testEvenStrategyClass(){
        fill(6);
        // even strategy
        assertEquals(2, this.list.next(new EvenStrategy()).get());
        assertEquals(4, this.list.next(new EvenStrategy()).get());
        assertEquals(6, this.list.next(new EvenStrategy()).get());
    }

    @Test
    void testMultipleOfStrategyClass(){
        fill(6);
        // multiple strategy
        final int element = 3;
        assertEquals(3, this.list.next(new MultipleOfStrategy(element)).get());
        assertEquals(6, this.list.next(new MultipleOfStrategy(element)).get());
        assertEquals(3, this.list.next(new MultipleOfStrategy(element)).get());
    }

    @Test
    void testEqualsStrategyClass(){
        fill(6);
        //multiple strategy
        final int element = 5;
        assertEquals(5, this.list.next(new EqualsStrategy(element)).get());
        assertEquals(5, this.list.next(new EqualsStrategy(element)).get());
    }
    */
